package com.droidkothon.joy.carparking;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.droidkothon.joy.carparking.fragments.AddPlace;
import com.droidkothon.joy.carparking.fragments.Home;
import com.droidkothon.joy.carparking.fragments.MyPlaces;
import com.droidkothon.joy.carparking.fragments.SearchFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.provider.UserDictionary.Words.APP_ID;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,GoogleApiClient.ConnectionCallbacks {

    FragmentManager fragmentManager;
    private static final int PERMISSION_ACCESS_COARSE_LOCATION=1;
    private GoogleApiClient googleApiClient;
    private NavigationView navigationView;
    private int chkINt=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        fragmentManager=getSupportFragmentManager();

        SharedPreferences preferences=getSharedPreferences("login", Context.MODE_PRIVATE);
        if (preferences.getBoolean("status",false)==true){
            navigationView.getMenu().getItem(4).setVisible(false);
        }
        else {
            navigationView.getMenu().getItem(5).setVisible(false);
            navigationView.getMenu().getItem(3).setVisible(false);
        }


        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();


        getLocation();

//        fragmentManager.beginTransaction()
//                .replace(R.id.content_frame, new Home())
//                .commit();
//        navigationView.getMenu().getItem(0).setChecked(true);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);


            //Toast.makeText(Main2Activity.this,String.valueOf(lastLocation.getLatitude()),Toast.LENGTH_SHORT).show();

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(lastLocation.getLatitude(),lastLocation.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


                Address address = addresses.get(0);


                //System.out.println(addresses.get(0));
                Log.d("ADRESSSS", String.valueOf(addresses.get(0)));

                SharedPreferences preferences=getSharedPreferences("loc",MODE_PRIVATE);
                SharedPreferences.Editor editor=preferences.edit();
                editor.putString("loc",address.getAddressLine(0));
                editor.putString("div",address.getLocality());
                editor.putString("lat", String.valueOf(address.getLatitude()));
                editor.putString("lon", String.valueOf(address.getLongitude()));
                editor.apply();

                //System.out.println(address.getLocality());

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (chkINt==0){
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, new Home())
                        .commit();
                navigationView.getMenu().getItem(0).setChecked(true);
                chkINt=1;

            }

        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
        ProgressDialog dialog=new ProgressDialog(this);
        dialog.setMessage("Please allow location");
        //dialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_add) {
            SharedPreferences preferences=getSharedPreferences("login", Context.MODE_PRIVATE);
            if (preferences.getBoolean("status",false)==true){
                fragmentManager.beginTransaction()
                        .replace(R.id.content_frame, new AddPlace())
                        .commit();
            }
            else {
                startActivity(new Intent(Main2Activity.this, LoginActivity.class));
            }

        }
        else if (id == R.id.nav_home) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new Home())
                    .commit();

        } else if (id == R.id.nav_login) {
            startActivity(new Intent(Main2Activity.this,LoginActivity.class));
        }
        else if (id == R.id.nav_logout) {
            SharedPreferences preferences=getSharedPreferences("login",MODE_PRIVATE);
            SharedPreferences.Editor editor=preferences.edit();
            editor.putString("email","null");
            editor.putBoolean("status",false);
            editor.commit();
            startActivity(new Intent(Main2Activity.this,Main2Activity.class));
            finish();

        }
        else if (id == R.id.nav_my_post) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new MyPlaces())
                    .commit();


        } else if (id == R.id.nav_search) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, new SearchFragment())
                    .commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getLocation(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION },
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // all good
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);

                } else {
                    Toast.makeText(this, "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

}
