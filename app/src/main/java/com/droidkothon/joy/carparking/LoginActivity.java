package com.droidkothon.joy.carparking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private Button button,btnReg;
    private EditText etEmail,etPass;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        etEmail=findViewById(R.id.edittext_email);
        etPass=findViewById(R.id.edittext_pass);
        button=findViewById(R.id.btn_login);
        btnReg=findViewById(R.id.btn_reg);

        dialog=new ProgressDialog(this);
        dialog.setMessage("Please wait");

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,CreateUserActivity.class));
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etEmail.getText().toString()==null || etPass.getText().toString()==null){

                    Toast.makeText(LoginActivity.this,"Please fill up all information",Toast.LENGTH_SHORT).show();

                }
                else {
                    login(etEmail.getText().toString(),etPass.getText().toString());

                }

            }
        });


    }

    private void login(final String email, final String pass){
        dialog.show();

        StringRequest request=new StringRequest(Request.Method.POST, "https://appincubatorbd.xyz/car-parking/login.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.cancel();

                if (response.equals("success")){
                    //Toast.makeText(LoginActivity.this,"Registration Successful",Toast.LENGTH_SHORT).show();
                    SharedPreferences preferences=getSharedPreferences("login",MODE_PRIVATE);
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putBoolean("status",true);
                    editor.putString("email",email);
                    editor.apply();
                    startActivity(new Intent(LoginActivity.this,Main2Activity.class));
                    finish();

                }
                else {
                    Toast.makeText(LoginActivity.this,"Something went wrong ! try later",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                Toast.makeText(LoginActivity.this,"Something went wrong ! try later",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("post_pass",pass);
                params.put("post_email",email);
                return params;
            }
        };
        request.setShouldCache(false);
        Volley.newRequestQueue(this).add(request);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(LoginActivity.this,Main2Activity.class));
    }
}
