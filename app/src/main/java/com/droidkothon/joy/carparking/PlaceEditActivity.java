package com.droidkothon.joy.carparking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.HashMap;
import java.util.Map;

public class PlaceEditActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner spinner;
    private ArrayAdapter adapter;
    private String strDiv,strLat,strLon,strEmail,strName,strCost,strID,strTotalSlot,strAvaSlot;
    private Button btnLoc,btnSubmit;
    private int PLACE_PICKER_REQUEST=2;
    private double lat,lon;
    private EditText etName,etCost,etTotalSlot,etAvaSlot;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        spinner=findViewById(R.id.spinner);
        btnLoc=findViewById(R.id.button_loc);
        etCost=findViewById(R.id.edittext_cost);
        etName=findViewById(R.id.edittext_name);
        btnSubmit=findViewById(R.id.button_submit);
        etTotalSlot=findViewById(R.id.edittext_total_slot);
        etAvaSlot=findViewById(R.id.edittext_available_slot);

        //--get intent--------------
        strDiv=getIntent().getStringExtra("div");
        strLat=getIntent().getStringExtra("lat");
        strLon=getIntent().getStringExtra("lon");
        strEmail=getIntent().getStringExtra("email");
        strName=getIntent().getStringExtra("name");
        strCost=getIntent().getStringExtra("cost");
        strID=getIntent().getStringExtra("id");
        strAvaSlot=getIntent().getStringExtra("ava_slot");
        strTotalSlot=getIntent().getStringExtra("total_slot");

        etCost.setText(strCost);
        etName.setText(strName);
        etAvaSlot.setText(strAvaSlot);
        etTotalSlot.setText(strTotalSlot);




        dialog=new ProgressDialog(this);
        dialog.setMessage("Please wait");

        adapter=ArrayAdapter.createFromResource(this,R.array.division,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(adapter);
        if (strDiv != null) {
            int spinnerPosition = adapter.getPosition(strDiv);
            spinner.setSelection(spinnerPosition);
        }
        spinner.setOnItemSelectedListener(this);

        btnLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                location();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etCost.getText().toString()==null || etName.getText().toString()==null || strDiv==null || strLat==null || strLon==null){
                    Toast.makeText(PlaceEditActivity.this,"Please fill up all information",Toast.LENGTH_SHORT).show();
                }
                else {
                    if (strDiv.equals("Select Division")){
                        Toast.makeText(PlaceEditActivity.this,"Please select your division",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        addPlace();
                    }
                }

            }
        });


    }

    private void addPlace(){

        dialog.show();
        StringRequest request=new StringRequest(Request.Method.POST, "https://appincubatorbd.xyz/car-parking/place_update.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("RESPONSE ",response);
                dialog.cancel();

                if (response.equals("success")){
                    Toast.makeText(PlaceEditActivity.this,"Place updated Successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(PlaceEditActivity.this,response,Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                Toast.makeText(PlaceEditActivity.this,"try later !",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("post_name",etName.getText().toString());
                params.put("post_cost",etCost.getText().toString());
                params.put("post_lat",strLat);
                params.put("post_lon",strLon);
                params.put("post_division",strDiv);
                params.put("post_email",strEmail);
                params.put("post_id",strID);
                params.put("post_ava_slot",etAvaSlot.getText().toString());
                params.put("post_total_slot",etTotalSlot.getText().toString());

                return params;
            }
        };
        request.setShouldCache(false);
        Volley.newRequestQueue(this).add(request);


    }
    private void location(){

        PlacePicker.IntentBuilder builder=new PlacePicker.IntentBuilder();
        try {
            Intent intent=builder.build(this);
            startActivityForResult(intent,PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==PLACE_PICKER_REQUEST && resultCode == RESULT_OK){

            Place place = PlacePicker.getPlace(data, this);
            CharSequence place_name=place.getAddress();
            //location= (String) place_name;
            lat= place.getLatLng().latitude;
            lon=place.getLatLng().longitude;

            strLat=String.valueOf(lat);
            strLon=String.valueOf(lon);

            //Toast.makeText(getActivity(),String.valueOf(lat),Toast.LENGTH_SHORT).show();


        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        strDiv=spinner.getSelectedItem().toString();


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
