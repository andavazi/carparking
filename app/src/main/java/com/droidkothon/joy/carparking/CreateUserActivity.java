package com.droidkothon.joy.carparking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class CreateUserActivity extends AppCompatActivity {

    private EditText etName,etPass,etmobile,etEmail;
    private Button button,btnLogin;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        etEmail=findViewById(R.id.edittext_email);
        etmobile=findViewById(R.id.edittext_phn_no);
        etPass=findViewById(R.id.edittext_pass);
        etName=findViewById(R.id.edittext_name);
        button=findViewById(R.id.btn_submit);
        btnLogin=findViewById(R.id.btn_login);

        dialog=new ProgressDialog(this);
        dialog.setMessage("Please wait");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etEmail.getText().toString()==null || etPass.getText().toString()==null || etmobile.getText().toString()==null || etName.getText().toString()==null){

                    Toast.makeText(CreateUserActivity.this,"Please fill up all information",Toast.LENGTH_SHORT).show();

                }
                else {

                    createUser(etName.getText().toString(),etEmail.getText().toString(),etPass.getText().toString(),etmobile.getText().toString());

                }


            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(CreateUserActivity.this,LoginActivity.class));

            }
        });
    }

    private void createUser(final String name, final String email, final String pass, final String mobile){

        dialog.show();

        StringRequest request=new StringRequest(Request.Method.POST, "https://appincubatorbd.xyz/car-parking/create-user.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.cancel();

                if (response.equals("same")){
                    Toast.makeText(CreateUserActivity.this,"This email id already registered",Toast.LENGTH_SHORT).show();
                }
                else if (response.equals("success")){
                    Toast.makeText(CreateUserActivity.this,"Registration Successful",Toast.LENGTH_SHORT).show();
                }
                else if (response.equals("error")){
                    Toast.makeText(CreateUserActivity.this,"Something went wrong ! try later",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                Toast.makeText(CreateUserActivity.this,"Something went wrong ! try later",Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("post_email",email);
                params.put("post_name",name);
                params.put("post_pass",pass);
                params.put("post_mobile",mobile);
                return params;

            }
        };
        request.setShouldCache(false);
        Volley.newRequestQueue(this).add(request);


    }


}
