package com.droidkothon.joy.carparking.fragments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.droidkothon.joy.carparking.DetailsActivity;
import com.droidkothon.joy.carparking.ListActivity;
import com.droidkothon.joy.carparking.R;
import com.droidkothon.joy.carparking.adapter.MainAdapter;
import com.droidkothon.joy.carparking.model.MainModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joy on 1/29/18.
 */

public class Home extends Fragment implements OnMapReadyCallback {

    private RecyclerView recyclerView;
    private MainAdapter adapter;
    private List<MainModel> list=new ArrayList<>();
    private ProgressDialog dialog;
    ArrayList<MainModel> markersArray = new ArrayList<MainModel>();
    GoogleMap googleMap,myMap;
    private MapView mapView;
    private CardView cardView;
    private Button btnDetails;
    private TextView tvTitle;
    private int id;
    private TextView tvList,tvDistance,tvMyLocation,tvTime,tvPrice;
    SharedPreferences preferences;
    private SupportMapFragment mapFragment;
    private static View view;
    private ImageView imgClose;
    private boolean isCardView=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view=LayoutInflater.from(getActivity()).inflate(R.layout.fragment_home,container,false);

        return view;
//        if (view != null) {
//            ViewGroup parent = (ViewGroup) view.getParent();
//            if (parent != null)
//                parent.removeView(view);
//        }
//        try {
//            view = inflater.inflate(R.layout.fragment_home, container, false);
//        } catch (InflateException e) {
//        /* map is already there, just return view as it is */
//        }
//        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        recyclerView=view.findViewById(R.id.recyclerView);
//
        cardView=view.findViewById(R.id.card_view);
        btnDetails=view.findViewById(R.id.button_details);
        tvTitle=view.findViewById(R.id.map_title);
        tvList=view.findViewById(R.id.tvList);
        tvDistance=view.findViewById(R.id.map_distance);
        tvMyLocation=view.findViewById(R.id.map_myLocation);
        tvTime=view.findViewById(R.id.map_time);
        imgClose=view.findViewById(R.id.imgClose);
        tvPrice=view.findViewById(R.id.map_price);

        //-dialog------
        dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Please wait..");
//
//        adapter=new MainAdapter(list,getActivity());
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView.setAdapter(adapter);

//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

//        mapView = (MapView)view.findViewById(R.id.mapView);
//        mapView.onCreate(savedInstanceState);
//        mapView.getMapAsync(this);
//        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        Date dt = new Date();
        int hours = dt.getHours();
        int minutes = dt.getMinutes();
        int seconds = dt.getSeconds();
        tvTime.setText(String.valueOf(hours)+" : "+String.valueOf(minutes));


        mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);




        preferences=getActivity().getSharedPreferences("loc", Context.MODE_PRIVATE);
        tvMyLocation.setText(preferences.getString("loc","null"));

        getPlace(preferences.getString("lat","null"),preferences.getString("lon","null"));


        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCardView){
                    cardView.setVisibility(View.GONE);
                    isCardView=false;
                }
            }
        });
        tvList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().startActivity(new Intent(getActivity(), ListActivity.class));

            }
        });
        btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainModel model=list.get(id);
                Intent intent=new Intent(getActivity(),DetailsActivity.class);

                intent.putExtra("name",model.getName());
                intent.putExtra("user",model.getUser());
                intent.putExtra("email",model.getEmail());
                intent.putExtra("lat",model.getLat());
                intent.putExtra("lon",model.getLon());
                intent.putExtra("div",model.getDivision());
                intent.putExtra("cost",model.getCost());
                intent.putExtra("mobile",model.getMobile());
                intent.putExtra("ava_slot",model.getAvaSlot());
                intent.putExtra("total_slot",model.getTotalSlot());
                intent.putExtra("img1",model.getImage1());
                intent.putExtra("img2",model.getImage2());
                intent.putExtra("img3",model.getImage3());

                getActivity().startActivity(intent);

            }
        });


    }

    private void getPlace(final String lat, final String lon){

        dialog.show();
        StringRequest request=new StringRequest(Request.Method.POST, "https://appincubatorbd.xyz/car-parking/get_place.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.cancel();

                Log.d("HOME RESPONCE",response);
                try {
                    JSONArray jsonArray=new JSONArray(response);

                    for (int i=0;i<jsonArray.length();i++){

                        MainModel model=new MainModel();

                        JSONObject jsonObject= (JSONObject) jsonArray.get(i);

                        model.setName(jsonObject.getString("name"));
                        model.setDivision(jsonObject.getString("division"));
                        model.setUser(jsonObject.getString("user"));
                        model.setCost(jsonObject.getString("cost"));
                        model.setLat(jsonObject.getString("lat"));
                        model.setLon(jsonObject.getString("lon"));
                        model.setEmail(jsonObject.getString("email"));
                        model.setMobile(jsonObject.getString("mobile"));
                        model.setAvaSlot(jsonObject.getString("ava_slot"));
                        model.setTotalSlot(jsonObject.getString("total_slot"));
                        model.setDistance(jsonObject.getString("distance"));
                        model.setImage1(jsonObject.getString("image1"));
                        model.setImage2(jsonObject.getString("image2"));
                        model.setImage3(jsonObject.getString("image3"));


                        MainModel mapModel=new MainModel();
                        mapModel.setLat(jsonObject.getString("lat"));
                        mapModel.setLon(jsonObject.getString("lon"));
                        markersArray.add(mapModel);

                        list.add(model);

//                        adapter.notifyDataSetChanged();

                        if (i==(jsonArray.length()-1)){
                            showMarker();
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                Toast.makeText(getActivity(),"Something goes wrong ",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("post_lat",lat);
                params.put("post_lon",lon);

                return params;
            }
        };
        request.setShouldCache(false);
        Volley.newRequestQueue(getActivity()).add(request);

    }

    private void showMarker(){
        Marker[] marker = new Marker[200];
        for(int i = 0 ; i < markersArray.size() ; i++ ) {

            marker[i]=createMarker(Double.parseDouble(markersArray.get(i).getLat()), Double.parseDouble(markersArray.get(i).getLon()), list.get(i).getName(), R.drawable.marker);

        }
        LatLng latLng=new LatLng(Double.parseDouble(markersArray.get(markersArray.size()-1).getLat()), Double.parseDouble(markersArray.get(markersArray.size()-1).getLon()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                if (marker.getTitle().equals("MyLocation")){

                }
                else {
                    cardView.setVisibility(View.VISIBLE);
                    isCardView=true;
                    tvTitle.setText(marker.getTitle());
                    //tvDistance.setText(String.valueOf(distance(Double.parseDouble(preferences.getString("lat","0.0")),Double.parseDouble(preferences.getString("lon","0.0")),marker.getPosition().latitude,marker.getPosition().longitude,0.0,0.0)));

                    for (int a=0;a<markersArray.size();a++){
                        if (marker.getTitle().equals(list.get(a).getName())) {

                            id=a;
                            //Toast.makeText(getActivity(),id,Toast.LENGTH_SHORT).show();
                            tvDistance.setText(list.get(a).getDistance()+" Km");
                            tvPrice.setText(list.get(a).getCost()+" TK/month");
                            break;

                        }
                    }
                }

                return false;
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap=googleMap;
        myMap=googleMap;
        createMyMarker(Double.parseDouble(preferences.getString("lat","null")),Double.parseDouble(preferences.getString("lon","null")),"MyLocation",R.drawable.marker);


    }
    protected Marker createMarker(double latitude, double longitude, String title, int iconResID) {

        return googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .icon(BitmapDescriptorFactory.defaultMarker()));
    }
    protected Marker createMyMarker(double latitude, double longitude, String title, int iconResID) {

        return myMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SupportMapFragment f = (SupportMapFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }

    @Override
    public void onPause() {
        super.onPause();
        SupportMapFragment f = (SupportMapFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }

    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }


}
