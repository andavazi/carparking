package com.droidkothon.joy.carparking.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.droidkothon.joy.carparking.LoginActivity;
import com.droidkothon.joy.carparking.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * Created by joy on 1/24/18.
 */

public class AddPlace extends Fragment implements AdapterView.OnItemSelectedListener {

    private Spinner spinner;
    private ArrayAdapter adapter;
    private String strDiv,strLat,strLon;
    private Button btnLoc,btnSubmit;
    private int PLACE_PICKER_REQUEST=10;
    private double lat,lon;
    private EditText etName,etCost,etTotalSlot,etAvaSlot;
    private SharedPreferences preferences;
    private ProgressDialog dialog;
    private static final int PICK_IMAGE_REQUEST1=1;
    private static final int PICK_IMAGE_REQUEST2=2;
    private static final int PICK_IMAGE_REQUEST3=3;
    private Bitmap bitmap1=null,bitmap2=null,bitmap3=null;
    private Uri filePath1,filePath2,filePath3;
    private ImageView img1,img2,img3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=LayoutInflater.from(getActivity()).inflate(R.layout.fragment_add_place,container,false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinner=view.findViewById(R.id.spinner);
        btnLoc=view.findViewById(R.id.button_loc);
        etCost=view.findViewById(R.id.edittext_cost);
        etName=view.findViewById(R.id.edittext_name);
        btnSubmit=view.findViewById(R.id.button_submit);
        etAvaSlot=view.findViewById(R.id.edittext_available_slot);
        etTotalSlot=view.findViewById(R.id.edittext_total_slot);
        img1=view.findViewById(R.id.img_1);
        img2=view.findViewById(R.id.img_2);
        img3=view.findViewById(R.id.img_3);

        preferences=getActivity().getSharedPreferences("login",Context.MODE_PRIVATE);

        dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Please wait");
        dialog.setCancelable(false);

        adapter=ArrayAdapter.createFromResource(getActivity(),R.array.division,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser(PICK_IMAGE_REQUEST1);

            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser(PICK_IMAGE_REQUEST2);

            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showFileChooser(PICK_IMAGE_REQUEST3);

            }
        });
        btnLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                location();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bitmap1==null ||bitmap2==null || bitmap3==null ||etCost.getText().toString()==null || etName.getText().toString()==null || strDiv==null || strLat==null || strLon==null || etTotalSlot.getText().toString()==null || etAvaSlot.getText().toString()==null){
                    Toast.makeText(getActivity(),"Please fill up all information",Toast.LENGTH_SHORT).show();
                }
                else {
                    if (strDiv.equals("Select Division")){
                        Toast.makeText(getActivity(),"Please select your division",Toast.LENGTH_SHORT).show();
                    }
                    else {

                        addPlace();
                    }
                }
            }
        });


    }

    private void addPlace(){

        dialog.show();
        StringRequest request=new StringRequest(Request.Method.POST, "http://appincubatorbd.xyz/car-parking/add-place.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("RESPONSE ",response);
                dialog.cancel();

                if (response.equals("success")){
                    Toast.makeText(getActivity(),"Place Added Successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(),response,Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                Toast.makeText(getActivity(),"try later !",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("post_name",etName.getText().toString());
                params.put("post_cost",etCost.getText().toString());
                params.put("post_lat",strLat);
                params.put("post_lon",strLon);
                params.put("post_division",strDiv);
                params.put("post_email",preferences.getString("email","null"));
                params.put("post_ava_slot",etAvaSlot.getText().toString());
                params.put("post_total_slot",etTotalSlot.getText().toString());
                params.put("post_image1",getStringImage(bitmap1));
                params.put("post_image2",getStringImage(bitmap2));
                params.put("post_image3",getStringImage(bitmap3));

                return params;
            }
        };
        request.setShouldCache(false);
        Volley.newRequestQueue(getActivity()).add(request);


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        strDiv=spinner.getSelectedItem().toString();


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void location(){

        PlacePicker.IntentBuilder builder=new PlacePicker.IntentBuilder();
        try {
            Intent intent=builder.build(getActivity());
            startActivityForResult(intent,PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        Log.e("REQUEST CODE",String.valueOf(requestCode));

        if (requestCode==PLACE_PICKER_REQUEST && resultCode == RESULT_OK){

            Place place = PlacePicker.getPlace(data, getActivity());
            CharSequence place_name=place.getAddress();
            //location= (String) place_name;
            lat= place.getLatLng().latitude;
            lon=place.getLatLng().longitude;

            strLat=String.valueOf(lat);
            strLon=String.valueOf(lon);

            //Toast.makeText(getActivity(),String.valueOf(lat),Toast.LENGTH_SHORT).show();
        }

        else if (requestCode == PICK_IMAGE_REQUEST1 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            filePath1 = data.getData();
            try {
                bitmap1 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),filePath1);
                img1.setImageBitmap(bitmap1);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else if (requestCode == PICK_IMAGE_REQUEST2 && resultCode == RESULT_OK && data != null && data.getData() != null){
            filePath2 = data.getData();
            try {
                bitmap2 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),filePath2);
                img2.setImageBitmap(bitmap2);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == PICK_IMAGE_REQUEST3 && resultCode == RESULT_OK && data != null && data.getData() != null){
            filePath3 = data.getData();
            try {
                bitmap3 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),filePath3);
                img3.setImageBitmap(bitmap3);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showFileChooser(int imgID){
        Intent intent=new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), imgID);

    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

}
