package com.droidkothon.joy.carparking;

import android.*;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DetailsActivity extends AppCompatActivity implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks,RoutingListener {

    private GoogleMap mMap;
    private TextView tvName, tvPlace, tvMobile,tvUser,tvCost,avaSlot,totalSlot,tvTime,tvDistance;
    private GoogleApiClient googleApiClient;
    private PolylineOptions rectOptions;
    private ArrayList<LatLng> pathPoint = new ArrayList<LatLng>();
    private ProgressDialog dialog;
    private LatLng latLng;
    private ImageView img1,img2,img3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tvMobile = findViewById(R.id.tvMobile);
        tvName = findViewById(R.id.tvName);
        tvPlace = findViewById(R.id.tvPlace);
        tvUser=findViewById(R.id.tvUser);
        tvCost=findViewById(R.id.tvCost);
        avaSlot=findViewById(R.id.tvAvaSlot);
        totalSlot=findViewById(R.id.tvTotalSlot);
        tvDistance=findViewById(R.id.tvDistance);
        tvTime=findViewById(R.id.tvTime);
        img1=findViewById(R.id.img_1);
        img2=findViewById(R.id.img_2);
        img3=findViewById(R.id.img_3);

        tvPlace.setText(getIntent().getStringExtra("div"));
        tvUser.setText(getIntent().getStringExtra("user"));
        tvMobile.setText("Mobile : "+getIntent().getStringExtra("mobile"));
        tvCost.setText("Cost / Month : "+getIntent().getStringExtra("cost"));
        tvName.setText(getIntent().getStringExtra("name"));
        totalSlot.setText("Total Slot: "+getIntent().getStringExtra("total_slot"));
        avaSlot.setText("Available Slot: "+getIntent().getStringExtra("ava_slot"));

        Picasso.with(this).load(getIntent().getStringExtra("img1")).into(img1);
        Picasso.with(this).load(getIntent().getStringExtra("img2")).into(img2);
        Picasso.with(this).load(getIntent().getStringExtra("img3")).into(img3);

        Log.e("IMAGE 1",getIntent().getStringExtra("img1"));
        Log.e("IMAGE 2",getIntent().getStringExtra("img2"));
        Log.e("IMAGE 3",getIntent().getStringExtra("img3"));

        dialog=new ProgressDialog(this);
        dialog.setMessage("Please wait..");

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        rectOptions = new PolylineOptions();


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

//        try{
//            // Add a marker in Sydney and move the camera
//            LatLng sydney = new LatLng(Double.parseDouble(getIntent().getStringExtra("lat")), Double.parseDouble(getIntent().getStringExtra("lon")));
//            mMap.addMarker(new MarkerOptions().position(sydney).title("Car Parking"));
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//            mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
//
//        }
//        catch (Exception e){
//
//        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            latLng=new LatLng(lastLocation.getLatitude(),lastLocation.getLongitude());

            mMap.addMarker(new MarkerOptions().position(latLng).title("Car Parking"));
            rectOptions.width(10);
            rectOptions.color(Color.BLUE);

            LatLng sydney = new LatLng(Double.parseDouble(getIntent().getStringExtra("lat")), Double.parseDouble(getIntent().getStringExtra("lon")));

            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .waypoints(sydney,latLng)
                    .key("AIzaSyCbxAEdCe4CN36RXJT4wwId1h6G24WS3IA")
                    .build();
            routing.execute();


        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        dialog.cancel();

    }

    @Override
    public void onRoutingFailure(RouteException e) {
        dialog.cancel();

    }

    @Override
    public void onRoutingStart() {
        dialog.show();

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> arrayList, int i) {
        dialog.cancel();

        LatLng sydney = new LatLng(Double.parseDouble(getIntent().getStringExtra("lat")), Double.parseDouble(getIntent().getStringExtra("lon")));

        rectOptions.addAll(arrayList.get(i).getPoints());
        tvTime.setText(arrayList.get(i).getDurationText());
        tvDistance.setText(arrayList.get(i).getDistanceText());

        mMap.addMarker(new MarkerOptions().position(sydney).title("Car Parking"));
        mMap.addPolyline(rectOptions);
        Location startLocation = new Location("startingPoint");
        startLocation.setLatitude(latLng.latitude);
        startLocation.setLongitude(latLng.longitude);

        Location endLocation = new Location("endingPoint");
        endLocation.setLatitude(sydney.latitude);
        endLocation.setLongitude(sydney.longitude);

        //get the bearing which will help in rotating the map.
        float targetBearing = startLocation.bearingTo(endLocation);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(sydney);
        builder.include(latLng);
        LatLngBounds bounds = builder.build();
        //define value for pading
        int paddng=20;
        //update camera
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,getResources().getDisplayMetrics().widthPixels,getResources().getDisplayMetrics().heightPixels,paddng);
        mMap.moveCamera(cu);

        //Now set this values in cameraposition
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(bounds.getCenter())
                .zoom(mMap.getCameraPosition().zoom-.5f)
                .bearing(targetBearing)
                .build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


    }

    @Override
    public void onRoutingCancelled() {

    }
}
