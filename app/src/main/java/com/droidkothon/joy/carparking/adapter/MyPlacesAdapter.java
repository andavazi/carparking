package com.droidkothon.joy.carparking.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.droidkothon.joy.carparking.DetailsActivity;
import com.droidkothon.joy.carparking.PlaceEditActivity;
import com.droidkothon.joy.carparking.R;
import com.droidkothon.joy.carparking.model.MainModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joy on 2/4/18.
 */

public class MyPlacesAdapter extends RecyclerView.Adapter<MyPlacesAdapter.MyViewHolder> {

    private List<MainModel> list = new ArrayList<>();
    private Context context;

    public MyPlacesAdapter(List<MainModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyPlacesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.my_places_item, parent, false);


        return new MyPlacesAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyPlacesAdapter.MyViewHolder holder, final int position) {

        final MainModel model = list.get(position);

        holder.name.setText(model.getName());
        holder.place.setText(model.getDivision());
        holder.totalSlot.setText("Total Slot : "+model.getTotalSlot());
        holder.avaSlot.setText("Available Slot : "+model.getAvaSlot());

        if (model.getStatus().equals("available")){
            holder.btnStatus.setText("Unavailable");
            holder.btnStatus.setBackgroundColor(Color.GREEN);
        }
        else {
            holder.btnStatus.setText("Available");
            holder.btnStatus.setBackgroundColor(Color.YELLOW);
        }

        holder.btnStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (model.getStatus().equals("available")){
                    updateStatus(model.getPlaceID(),"unavailable",holder);
                }
                else {
                    updateStatus(model.getPlaceID(),"available",holder);
                }



            }
        });

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainModel model = list.get(position);
                Intent intent = new Intent(context, PlaceEditActivity.class);

                intent.putExtra("name", model.getName());
                intent.putExtra("email", model.getEmail());
                intent.putExtra("lat", model.getLat());
                intent.putExtra("lon", model.getLon());
                intent.putExtra("div", model.getDivision());
                intent.putExtra("cost", model.getCost());
                intent.putExtra("id", model.getPlaceID());
                intent.putExtra("ava_slot",model.getAvaSlot());
                intent.putExtra("total_slot",model.getTotalSlot());

                context.startActivity(intent);


            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deletePlace(model.getPlaceID(),list,position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private TextView name, place,avaSlot,totalSlot;
        Button btnEdit,btnDelete,btnStatus;

        public MyViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.tvName);
            place = itemView.findViewById(R.id.tvPlace);
            btnDelete=itemView.findViewById(R.id.button_delete);
            btnEdit=itemView.findViewById(R.id.button_edit);
            btnStatus=itemView.findViewById(R.id.button_status);
            avaSlot=itemView.findViewById(R.id.tvAvaSlot);
            totalSlot=itemView.findViewById(R.id.tvTotalSlot);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            //Toast.makeText(context,"CLICKED !!",Toast.LENGTH_SHORT).show();

            MainModel model = list.get(getAdapterPosition());
            Intent intent = new Intent(context, DetailsActivity.class);

            intent.putExtra("name", model.getName());
            intent.putExtra("user", model.getUser());
            intent.putExtra("email", model.getEmail());
            intent.putExtra("lat", model.getLat());
            intent.putExtra("lon", model.getLon());
            intent.putExtra("div", model.getDivision());
            intent.putExtra("cost", model.getCost());
            intent.putExtra("mobile", model.getMobile());
            intent.putExtra("ava_slot",model.getAvaSlot());
            intent.putExtra("total_slot",model.getTotalSlot());

            context.startActivity(intent);


        }
    }

    private void deletePlace(final String id, final List<MainModel> list, final int pos){


        StringRequest stringRequest=new StringRequest(Request.Method.POST, "https://appincubatorbd.xyz/car-parking/delete_place.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.equals("success")){
                    list.remove(pos);
                    notifyDataSetChanged();
                    Toast.makeText(context,response,Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(context,response,Toast.LENGTH_SHORT).show();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"something goes wrong ! try later",Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("postid",id);

                return params;

            }
        };
        stringRequest.setShouldCache(false);
        Volley.newRequestQueue(context).add(stringRequest);

    }
    private void updateStatus(final String id, final String status, final MyPlacesAdapter.MyViewHolder holder){


        StringRequest stringRequest=new StringRequest(Request.Method.POST, "https://appincubatorbd.xyz/car-parking/update-status.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.equals("success")){

                    if (status.equals("available")){
                        holder.btnStatus.setText("Available");
                        holder.btnStatus.setBackgroundColor(Color.YELLOW);
                    }
                    else {
                        holder.btnStatus.setText("Unavailable");
                        holder.btnStatus.setBackgroundColor(Color.GREEN);
                    }
                    Toast.makeText(context,response,Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(context,response,Toast.LENGTH_SHORT).show();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"something goes wrong ! try later",Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("post_id",id);
                params.put("post_status",status);

                return params;

            }
        };
        stringRequest.setShouldCache(false);
        Volley.newRequestQueue(context).add(stringRequest);

    }
}
