package com.droidkothon.joy.carparking.fragments;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.droidkothon.joy.carparking.R;
import com.droidkothon.joy.carparking.adapter.MainAdapter;
import com.droidkothon.joy.carparking.adapter.MyPlacesAdapter;
import com.droidkothon.joy.carparking.model.MainModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joy on 2/4/18.
 */

public class MyPlaces extends Fragment {

    private RecyclerView recyclerView;
    private MyPlacesAdapter adapter;
    private List<MainModel> list = new ArrayList<>();
    private String email;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_my_places, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView=view.findViewById(R.id.recyclerView);

        adapter=new MyPlacesAdapter(list,getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        SharedPreferences preferences=getActivity().getSharedPreferences("login",Context.MODE_PRIVATE);
        email=preferences.getString("email","null");
        getPlaces(email);

    }

    private void getPlaces(final String e){

        StringRequest request=new StringRequest(Request.Method.POST, "https://appincubatorbd.xyz/car-parking/my_places.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("MY JSON",response);

                try {
                    JSONArray jsonArray=new JSONArray(response);

                    for (int i=0;i<jsonArray.length();i++){

                        MainModel model=new MainModel();

                        JSONObject jsonObject= (JSONObject) jsonArray.get(i);

                        model.setName(jsonObject.getString("name"));
                        model.setDivision(jsonObject.getString("division"));
                        model.setUser(jsonObject.getString("user"));
                        model.setCost(jsonObject.getString("cost"));
                        model.setLat(jsonObject.getString("lat"));
                        model.setLon(jsonObject.getString("lon"));
                        model.setEmail(jsonObject.getString("email"));
                        model.setMobile(jsonObject.getString("mobile"));
                        model.setStatus(jsonObject.getString("status"));
                        model.setPlaceID(jsonObject.getString("place_id"));
                        model.setAvaSlot(jsonObject.getString("ava_slot"));
                        model.setTotalSlot(jsonObject.getString("total_slot"));

                        list.add(model);

                        adapter.notifyDataSetChanged();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Something goes wrong ",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("post_email",e);

                return params;
            }


        };
        request.setShouldCache(false);
        Volley.newRequestQueue(getActivity()).add(request);

    }
}
