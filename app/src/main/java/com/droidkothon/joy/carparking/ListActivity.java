package com.droidkothon.joy.carparking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.droidkothon.joy.carparking.adapter.MainAdapter;
import com.droidkothon.joy.carparking.model.MainModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MainAdapter adapter;
    private List<MainModel> list=new ArrayList<>();
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        recyclerView=findViewById(R.id.recyclerView);

        adapter=new MainAdapter(list,ListActivity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        dialog=new ProgressDialog(this);
        dialog.setMessage("Please wait..");

        SharedPreferences preferences=getSharedPreferences("loc", Context.MODE_PRIVATE);

        getPlace(preferences.getString("lat","null"),preferences.getString("lon","null"));

    }



    private void getPlace(final String lat, final String lon){

        dialog.show();
        StringRequest request=new StringRequest(Request.Method.POST, "https://appincubatorbd.xyz/car-parking/get_place.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.cancel();

                Log.d("HOME RESPONCE",response);
                try {
                    JSONArray jsonArray=new JSONArray(response);

                    for (int i=0;i<jsonArray.length();i++){

                        MainModel model=new MainModel();

                        JSONObject jsonObject= (JSONObject) jsonArray.get(i);

                        model.setName(jsonObject.getString("name"));
                        model.setDivision(jsonObject.getString("division"));
                        model.setUser(jsonObject.getString("user"));
                        model.setCost(jsonObject.getString("cost"));
                        model.setLat(jsonObject.getString("lat"));
                        model.setLon(jsonObject.getString("lon"));
                        model.setEmail(jsonObject.getString("email"));
                        model.setMobile(jsonObject.getString("mobile"));
                        model.setAvaSlot(jsonObject.getString("ava_slot"));
                        model.setTotalSlot(jsonObject.getString("total_slot"));
                        model.setImage1(jsonObject.getString("image1"));
                        model.setImage2(jsonObject.getString("image2"));
                        model.setImage3(jsonObject.getString("image3"));

                        list.add(model);

                        adapter.notifyDataSetChanged();


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                Toast.makeText(ListActivity.this,"Something goes wrong ",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<>();
                params.put("post_lat",lat);
                params.put("post_lon",lon);

                return params;
            }
        };
        request.setShouldCache(false);
        Volley.newRequestQueue(this).add(request);

    }
}
