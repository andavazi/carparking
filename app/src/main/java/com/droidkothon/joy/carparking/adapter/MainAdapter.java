package com.droidkothon.joy.carparking.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.droidkothon.joy.carparking.DetailsActivity;
import com.droidkothon.joy.carparking.R;
import com.droidkothon.joy.carparking.model.MainModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joy on 1/29/18.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder>   {

    private List<MainModel> list=new ArrayList<>();
    private Context context;

    public MainAdapter(List<MainModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.card_item,parent,false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        MainModel model=list.get(position);

        holder.name.setText(model.getName());
        holder.place.setText(model.getDivision());
        holder.totalSlot.setText("Total Slot : "+model.getTotalSlot());
        holder.avaSlot.setText("Available Slot : "+model.getAvaSlot());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        private TextView name,place,avaSlot,totalSlot;

        public MyViewHolder(View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.tvName);
            place=itemView.findViewById(R.id.tvPlace);
            avaSlot=itemView.findViewById(R.id.tvAvaSlot);
            totalSlot=itemView.findViewById(R.id.tvTotalSlot);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            //Toast.makeText(context,"CLICKED !!",Toast.LENGTH_SHORT).show();

            MainModel model=list.get(getAdapterPosition());
            Intent intent=new Intent(context,DetailsActivity.class);

            intent.putExtra("name",model.getName());
            intent.putExtra("user",model.getUser());
            intent.putExtra("email",model.getEmail());
            intent.putExtra("lat",model.getLat());
            intent.putExtra("lon",model.getLon());
            intent.putExtra("div",model.getDivision());
            intent.putExtra("cost",model.getCost());
            intent.putExtra("mobile",model.getMobile());
            intent.putExtra("ava_slot",model.getAvaSlot());
            intent.putExtra("total_slot",model.getTotalSlot());
            intent.putExtra("img1",model.getImage1());
            intent.putExtra("img2",model.getImage2());
            intent.putExtra("img3",model.getImage3());

            context.startActivity(intent);




        }
    }



}